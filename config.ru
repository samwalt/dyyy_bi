require 'sinatra'
require 'active_record'
require 'yaml'
require 'json'
require 'http'
require 'base64'

$container = {}
params = YAML::load(IO.read('params.yml'))
$container.merge!(params[ENV['RACK_ENV']])


# 加载绝对路径下的代码文件
def load_files(p)
    q = Queue.new
    q << p
    while q.size > 0
        path = q.pop
        files = Dir.children(path)
        files.each do |file|
            sub_path = path + '/' + file
            if File.directory?(sub_path)
                q << sub_path
            else
                require sub_path
            end
        end
    end
    q = nil
end

ActiveRecord::Base.default_timezone = :local

configure :development do
    set :show_exceptions, false

    $logger = Logger.new(STDOUT)
    # 显示sql
    ActiveRecord::Base.logger = $logger
end

configure :test, :production do
    # set :haml, { :ugly=>true }
    # set :clean_trace, true
    set :logging, true
    log_file = File.new('production.log', 'a')
    log_file.sync = true
    $logger = Logger.new(log_file, 'weekly')
    $logger.level = Logger::INFO
end

options '*' do
    headers "Access-Control-Allow-Origin" => "*"
    headers "Access-Control-Allow-Methods" => "PUT, POST, GET, OPTIONS, DELETE"
    headers "Access-Control-Allow-Headers" => "X-Requested-With,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token"
    headers "Access-Control-Max-Age" => "3600"
end

before do 
    headers "Access-Control-Allow-Origin" => "*"
    headers "Access-Control-Allow-Methods" => "PUT, POST, GET, OPTIONS, DELETE"
    headers "Access-Control-Allow-Headers" => "X-Requested-With,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token"
    headers "Access-Control-Max-Age" => "3600"
    if (!request.path.include?('/api') && !request.path.eql?('/login')) && !request.request_method.eql?('OPTIONS')
        token = request['token']
        halt 400, '请先登录' if token.nil? || 'dyyy' != Base64.decode64(token)
    end
end

after do
    headers "Content-Type" => "application/json"
end

error 400..500 do
    if response.status == 400
        {"message": response.body[0]}.to_json
    else
        $logger.error(env['sinatra.error'])
        {"message": env['sinatra.error'].message}.to_json
    end
end

load_files(File.expand_path('../app/db', __FILE__))
load_files(File.expand_path('../app/models', __FILE__))
load_files(File.expand_path('../app/lib', __FILE__))
load_files(File.expand_path('../app/api', __FILE__))

set :protection, :except => :json_csrf
run Sinatra::Application