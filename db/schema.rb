# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_17_055605) do

  create_table "bl_artistk", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "deptno", limit: 200, comment: "门店id"
    t.decimal "artiid", precision: 22, comment: "商品id"
    t.string "articode", limit: 20, comment: "商品编码"
    t.string "artibarcode", limit: 200, comment: "条形码"
    t.string "artiname", limit: 200, comment: "商品通用名"
    t.string "artiabbr", limit: 200, comment: "商品名"
    t.string "artispec", limit: 200, comment: "规格"
    t.string "artiunit", limit: 20, comment: "单位"
    t.string "manufactory", limit: 200, comment: "生产厂家"
    t.decimal "retailprice", precision: 16, scale: 6, comment: "零售价"
    t.string "condition", limit: 100, comment: "存储条件"
    t.string "yblx", limit: 20, comment: "医保类型"
    t.string "ybsx", limit: 20, comment: "医保属性"
    t.string "artinameadd", limit: 200, comment: "医保商品通用名"
    t.string "artiabbradd", limit: 200, comment: "医保商品名"
    t.string "artispecadd", limit: 120, comment: "医保规格"
    t.string "artiunitadd", limit: 20, comment: "医保单位"
    t.string "manufactoryadd", limit: 200, comment: "医保厂家"
    t.decimal "ybarti", precision: 22, comment: "是否医保药"
    t.string "ybcode", limit: 20, comment: "医保编码"
    t.decimal "stkqty", precision: 16, scale: 6, comment: "库存数量"
    t.string "articlasscode", limit: 100, comment: "商品分类"
    t.string "authorize", limit: 80, comment: "批准文号"
  end

  create_table "bl_drug_image", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "artiid", precision: 22, comment: "商品id"
    t.string "path", limit: 200, comment: "图片路径"
    t.string "type", limit: 50, comment: "图片类型"
  end

  create_table "bl_face_image", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "image_path", limit: 200, comment: "人脸标准图片路径"
    t.string "mobile", limit: 11, comment: "手机号"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bl_order", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "deptno", limit: 100, null: false, comment: "门店id"
    t.string "orderid", limit: 300, null: false, comment: "销售单号"
    t.decimal "saleamt", precision: 16, scale: 4, null: false, comment: "整单交易金额"
    t.datetime "createtime", null: false, comment: "交易时间"
    t.string "cashtype", limit: 40, null: false, comment: "收款类型"
    t.string "posid", limit: 40, null: false, comment: "设备号"
    t.string "memcardno", limit: 100, comment: "会员卡号"
    t.string "tranid", limit: 200, comment: "支付订单号"
  end

  create_table "bl_order_item", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "deptno", limit: 100, null: false, comment: "门店id"
    t.string "orderid", limit: 300, null: false, comment: "销售单号"
    t.string "orderdetailid", limit: 300, null: false, comment: "销售明细流水号"
    t.string "artiid", limit: 300, null: false, comment: "商品id"
    t.decimal "saleqty", precision: 16, scale: 2, comment: "销售数量"
    t.decimal "saleprice", precision: 16, scale: 4, comment: "销售单价"
    t.decimal "saleamt", precision: 16, scale: 2, comment: "销售金额"
    t.datetime "createtime", null: false, comment: "交易时间"
    t.integer "status", default: 0, null: false
    t.string "batchcode", limit: 100, comment: "批号"
    t.date "validdate", comment: "有效期"
  end

  create_table "carelinker_measurement", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "measurement_id", limit: 32, comment: "测量记录ID"
    t.string "branch_storeid", limit: 32, comment: "分店ID，是蓝信康对各分店定义维护的ID"
    t.string "user_name", limit: 32, comment: "用户ID，用来唯一标识用户"
    t.string "ms_card", limit: 32, comment: "用户会员卡号"
    t.integer "channel", limit: 1, comment: "（血压）对应血压计上的用户一和用户二，值为1或2"
    t.integer "systolic", limit: 1, comment: "（血压）收缩压（高压）", unsigned: true
    t.integer "diastolic", limit: 1, comment: "（血压）舒张压（低压）", unsigned: true
    t.integer "heart_rate", limit: 1, comment: "（血压）心率", unsigned: true
    t.decimal "bloodsugar", precision: 8, scale: 2, comment: "（血糖）血糖值"
    t.integer "stage", limit: 1, comment: "（血糖）测量时段；1-早起，2-餐前，3-.餐后，4-睡前", unsigned: true
    t.decimal "tc", precision: 8, scale: 2, comment: "（血糖）血糖值"
    t.decimal "tg", precision: 8, scale: 2, comment: "（血脂）甘油三脂"
    t.decimal "hdl", precision: 8, scale: 2, comment: "（血脂）高密度脂蛋白"
    t.decimal "ldl", precision: 8, scale: 2, comment: "（血脂）低密度脂蛋白"
    t.datetime "acquisition_time", comment: "血压测量时间"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "carelinker_user", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "branch_storeid", limit: 32, comment: "分店ID，是蓝信康对各分店定义维护的ID"
    t.string "user_name", limit: 32, comment: "用户ID，用来唯一标识用户"
    t.string "ms_card", limit: 32, comment: "用户会员卡号"
    t.date "birthday", comment: "生日"
    t.decimal "weight", precision: 8, scale: 2, comment: "用户体重，单位kg"
    t.decimal "height", precision: 8, scale: 2, comment: "用户身高，单位cm"
    t.integer "gender", limit: 1, comment: "用户性别，1-男性，2-女性"
    t.string "name", limit: 32, comment: "用户姓名"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hk_customer_detail", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "tenant_id", limit: 64, comment: "租户id"
    t.string "store_id", limit: 32, comment: "门店id"
    t.string "store_name", limit: 64, comment: "门店名称"
    t.string "device_serial", limit: 64, comment: "设备序列号"
    t.integer "channel_no", limit: 2, comment: "通道号"
    t.string "resource_name", limit: 64, comment: "资源名称"
    t.integer "user_type", limit: 1, comment: "用户类型(0:全部1:会员 2:员工 3惯偷 4:回头客5:普通顾客)"
    t.string "face_token", limit: 64, comment: "人脸图token"
    t.string "face_url", comment: "人脸图url"
    t.string "standard_token", limit: 64, comment: "标准图token"
    t.string "standard_url", comment: "标准图url"
    t.string "bg_url", comment: "背景图url"
    t.string "similarity", limit: 32, comment: "相似度"
    t.integer "sex", limit: 1
    t.integer "age", limit: 1
    t.integer "glasses", limit: 1, comment: "是否戴眼镜0:无眼镜 1:有眼镜"
    t.bigint "face_time", comment: "人脸图时间"
    t.datetime "created_at"
    t.string "mobile", limit: 11, default: ""
    t.index ["face_time", "device_serial"], name: "index_hk_customer_detail_on_face_time_and_device_serial", unique: true
  end

  create_table "hk_customer_statistics", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "store_id", limit: 32, comment: "门店id"
    t.date "updated_at"
    t.string "k", limit: 10, comment: "数据名"
    t.integer "v", limit: 2, comment: "数据值"
    t.integer "user_type", limit: 1, comment: "用户类型(0:全部1:会员 2:员工 3惯偷 4:回头客5:普通顾客)"
    t.string "t", limit: 10, comment: "数据类型"
    t.index ["store_id", "updated_at", "k", "user_type", "t"], name: "idx_uniq", unique: true
  end

  create_table "hk_device", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "device_id", limit: 32, comment: "设备id"
    t.string "device_name", limit: 32, comment: "设备名称"
    t.string "device_model", limit: 32, comment: "设备型号"
    t.string "device_serial", limit: 16, comment: "设备序列号"
    t.string "channel_id", limit: 32, comment: "通道id"
    t.string "channel_name", limit: 64, comment: "通道名称"
    t.integer "channel_no", comment: "通道号"
    t.string "channel_status", limit: 1, comment: "状态 0：离线，1：在线"
    t.string "channel_pic_url", comment: "通道封面图片URL"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hp_item", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "store_id", comment: "门店id"
    t.integer "device_id", comment: "货架id"
    t.integer "cell_id", comment: "商品标定的区域编号"
    t.integer "sku_id", comment: "商品的sku id"
    t.string "item_name", limit: 50, comment: "商品名称"
    t.datetime "updated_at"
  end

  create_table "hp_item_conversion", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "store_id", comment: "门店id"
    t.integer "device_id", comment: "货架id"
    t.integer "cell_id", comment: "商品标定的区域编号"
    t.integer "sku_id", comment: "商品的sku id"
    t.integer "interact_people", limit: 2, comment: "与货架交互的人数"
    t.integer "interact_count", limit: 2, comment: "与货架交互的次数"
    t.datetime "updated_at"
    t.index ["store_id", "device_id", "cell_id", "sku_id", "updated_at"], name: "idx_uniq", unique: true
  end

  create_table "hp_shelf", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "store_id", comment: "门店id"
    t.integer "device_id", comment: "货架id"
    t.string "region", limit: 50, comment: "地区"
    t.string "province", limit: 50, comment: "省份"
    t.string "city", limit: 50, comment: "城市"
    t.string "store_name", limit: 50, comment: "店铺名"
    t.string "shelf_name", limit: 50, comment: "货架名"
    t.datetime "updated_at"
  end

  create_table "hp_shelf_conversion", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "store_id", comment: "门店id"
    t.integer "device_id", comment: "货架id"
    t.integer "pass_people", limit: 2, comment: "经过货架的人数"
    t.integer "stay_people", limit: 2, comment: "在货架前停留的人数"
    t.integer "interact_people", limit: 2, comment: "与货架交互的人数"
    t.datetime "updated_at"
    t.index ["store_id", "device_id", "updated_at"], name: "idx_uniq", unique: true
  end

  create_table "material", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "original_name", limit: 50, comment: "原文件名"
    t.string "path", limit: 256, comment: "oss的路径"
    t.integer "file_type", limit: 1, comment: "1 图片；2 视频；3 二维码图片"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "material_store", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "material_id"
    t.integer "store_id"
    t.integer "file_type", limit: 1, comment: "1 图片；2 视频；3 二维码图片"
    t.integer "enabled", limit: 1, default: 0, comment: "1 启用；0 禁用"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "store", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "code", limit: 10, comment: "门店编码"
    t.string "name", limit: 20, comment: "门店名"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "synchronize_record", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "source", limit: 20, comment: "数据来源"
    t.string "api", limit: 20, comment: "api名称"
    t.datetime "sync_time", comment: "同步时间"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "token", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "store_id", limit: 10, comment: "门店id"
    t.string "token", limit: 60
    t.date "valid_date", comment: "有效日期"
    t.string "from", limit: 10, comment: "token从何处颁发"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
