class CreateHpShelfConversion < ActiveRecord::Migration[5.2]
  def change
    create_table :hp_shelf_conversion do |t|
      t.integer :store_id, :comment=>'门店id'
      t.integer :device_id, :comment=>'货架id'
      t.column :pass_people, 'smallint', :comment=>'经过货架的人数'
      t.column :stay_people, 'smallint', :comment=>'在货架前停留的人数'
      t.column :interact_people, 'smallint', :comment=>'与货架交互的人数'
      t.datetime :updated_at
    end
    add_index :hp_shelf_conversion, [:store_id, :device_id, :updated_at], unique: true, :name=>'idx_uniq'
  end
end
