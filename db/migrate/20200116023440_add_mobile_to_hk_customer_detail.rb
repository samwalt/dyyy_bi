class AddMobileToHkCustomerDetail < ActiveRecord::Migration[5.2]
  def change
    add_column :hk_customer_detail, :mobile, :string, :limit=>11, :default=>''
  end
end
