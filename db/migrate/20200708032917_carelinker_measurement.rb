class CarelinkerMeasurement < ActiveRecord::Migration[5.2]
  def change
    create_table :carelinker_measurement do |t|
      t.string :measurement_id, :limit=>32, :comment=>'测量记录ID'
      t.string :branch_storeid, :limit=>32, :comment=>'分店ID，是蓝信康对各分店定义维护的ID'
      t.string :user_name, :limit=>32, :comment=>'用户ID，用来唯一标识用户'
      t.string :ms_card, :limit=>32, :comment=>'用户会员卡号'
      t.column :channel, 'tinyint', :comment=>'（血压）对应血压计上的用户一和用户二，值为1或2'
      t.column :systolic, 'tinyint unsigned', :comment=>'（血压）收缩压（高压）'
      t.column :diastolic, 'tinyint unsigned', :comment=>'（血压）舒张压（低压）'
      t.column :heart_rate, 'tinyint unsigned', :comment=>'（血压）心率'
      t.decimal :bloodsugar, :precision=>8, :scale=>2, :comment=>'（血糖）血糖值'
      t.column :stage, 'tinyint unsigned', :comment=>'（血糖）测量时段；1-早起，2-餐前，3-.餐后，4-睡前'
      t.decimal :tc, :precision=>8, :scale=>2, :comment=>'（血糖）血糖值'
      t.decimal :tg, :precision=>8, :scale=>2, :comment=>'（血脂）甘油三脂'
      t.decimal :hdl, :precision=>8, :scale=>2,  :comment=>'（血脂）高密度脂蛋白'
      t.decimal :ldl, :precision=>8, :scale=>2,  :comment=>'（血脂）低密度脂蛋白'
      t.datetime :acquisition_time, :comment=>'血压测量时间' 
      t.timestamps
    end
  end
end
