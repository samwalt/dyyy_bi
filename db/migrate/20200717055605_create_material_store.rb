class CreateMaterialStore < ActiveRecord::Migration[5.2]
  def change
    create_table :material_store do |t|
      t.integer :material_id
      t.integer :store_id
      t.column :file_type, 'tinyint', :comment=>'1 图片；2 视频；3 二维码图片'
      t.column :enabled, 'tinyint', :default=>0, :comment=>'1 启用；0 禁用'
      t.timestamps
    end
  end
end
