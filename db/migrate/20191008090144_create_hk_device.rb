class CreateHkDevice < ActiveRecord::Migration[5.2]
  def change
    create_table :hk_device do |t|
      t.string :device_id, :limit=>32, :comment=>'设备id'
      t.string :device_name, :limit=>32, :comment=>'设备名称'
      t.string :device_model, :limit=>32, :comment=>'设备型号'
      t.string :device_serial, :limit=>16, :comment=>'设备序列号'
      t.string :channel_id, :limit=>32, :comment=>'通道id'
      t.string :channel_name, :limit=>64, :comment=>'通道名称'
      t.integer :channel_no, :comment=>'通道号'
      t.string :channel_status, :limit=>1, :comment=>'状态 0：离线，1：在线'
      t.string :channel_pic_url, :limit=>255, :comment=>'通道封面图片URL'
      t.timestamps
    end
  end
end
