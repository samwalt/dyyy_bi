class CreateToken < ActiveRecord::Migration[5.2]
  def change
    create_table :token do |t|
      t.string :store_id, :limit=>10, :comment=>'门店id'
      t.string :token, :limit=>60
      t.date :valid_date, :comment=>'有效日期'
      t.string :from, :limit=>10, :comment=>'token从何处颁发'
      t.timestamps
    end
  end
end
