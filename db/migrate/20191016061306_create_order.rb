class CreateOrder < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_order, {id: false} do |t|
      t.string :deptno, :limit=>100, null: false, :comment=>'门店id'
      t.string :orderid, :limit=>300, null: false, :comment=>'销售单号'
      t.decimal :saleamt, :precision=>16, :scale=>4, null: false, :comment=>'整单交易金额'
      t.datetime :createtime, null: false, :comment=>'交易时间'
      t.string :cashtype, :limit=>40, null: false, :comment=>'收款类型'
      t.string :posid, :limit=>40, null: false, :comment=>'设备号'
      t.string :memcardno, :limit=>100, :comment=>'会员卡号'
      t.string :tranid, :limit=>200, :comment=>'支付订单号'
    end
  end
end
