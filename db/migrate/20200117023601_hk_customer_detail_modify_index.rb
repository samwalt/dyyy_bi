class HkCustomerDetailModifyIndex < ActiveRecord::Migration[5.2]
  def change
    remove_index :hk_customer_detail, column: [:device_serial, :face_time]
    add_index :hk_customer_detail, [:face_time, :device_serial], unique: true
  end
end
