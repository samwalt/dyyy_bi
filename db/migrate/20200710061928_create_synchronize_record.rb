class CreateSynchronizeRecord < ActiveRecord::Migration[5.2]
  def change
    create_table :synchronize_record do |t|
      t.string :source, :limit=>20, :comment=>'数据来源'
      t.string :api, :limit=>20, :comment=>'api名称'
      t.datetime :sync_time, :comment=>'同步时间'
      t.timestamps
    end
  end
end
