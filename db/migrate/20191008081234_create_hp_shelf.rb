class CreateHpShelf < ActiveRecord::Migration[5.2]
  def change
    create_table :hp_shelf do |t|
      t.integer :store_id, :comment=>'门店id'
      t.integer :device_id, :comment=>'货架id'
      t.string :region, :limit=>50, :comment=>'地区'
      t.string :province, :limit=>50, :comment=>'省份'
      t.string :city, :limit=>50, :comment=>'城市'
      t.string :store_name, :limit=>50, :comment=>'店铺名'
      t.string :shelf_name, :limit=>50, :comment=>'货架名'
      t.datetime :updated_at
    end
  end
end
