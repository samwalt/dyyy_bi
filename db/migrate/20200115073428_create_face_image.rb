class CreateFaceImage < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_face_image do |t|
      t.string :image_path, :limit=>200, :comment=>'人脸标准图片路径'
      t.string :mobile, :limit=>11, :comment=>'手机号'
      t.timestamps
    end
  end
end
