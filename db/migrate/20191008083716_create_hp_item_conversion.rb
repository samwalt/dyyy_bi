class CreateHpItemConversion < ActiveRecord::Migration[5.2]
  def change
    create_table :hp_item_conversion do |t|
      t.integer :store_id, :comment=>'门店id'
      t.integer :device_id, :comment=>'货架id'
      t.integer :cell_id, :comment=>'商品标定的区域编号'
      t.integer :sku_id, :comment=>'商品的sku id'
      t.column :interact_people, 'smallint', :comment=>'与货架交互的人数'
      t.column :interact_count, 'smallint', :comment=>'与货架交互的次数'
      t.datetime :updated_at
    end
    add_index :hp_item_conversion, [:store_id, :device_id, :cell_id, :sku_id, :updated_at], unique: true, :name=>'idx_uniq'
  end
end
