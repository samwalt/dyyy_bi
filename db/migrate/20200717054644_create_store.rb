class CreateStore < ActiveRecord::Migration[5.2]
  def change
    create_table :store do |t|
      t.string :code, :limit=>10, :comment=>'门店编码'
      t.string :name, :limit=>20, :comment=>'门店名'
      t.timestamps
    end
  end
end
