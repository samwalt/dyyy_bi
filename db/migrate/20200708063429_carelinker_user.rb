class CarelinkerUser < ActiveRecord::Migration[5.2]
  def change
    create_table :carelinker_user do |t|
      t.string :branch_storeid, :limit=>32, :comment=>'分店ID，是蓝信康对各分店定义维护的ID'
      t.string :user_name, :limit=>32, :comment=>'用户ID，用来唯一标识用户'
      t.string :ms_card, :limit=>32, :comment=>'用户会员卡号'
      t.date :birthday, :comment=>'生日'
      t.decimal :weight, :precision=>8, :scale=>2, :comment=>'用户体重，单位kg'
      t.decimal :height, :precision=>8, :scale=>2, :comment=>'用户身高，单位cm'
      t.column :gender, 'tinyint', :comment=>'用户性别，1-男性，2-女性'
      t.string :name, :limit=>32, :comment=>'用户姓名'

      t.timestamps
    end
  end
end
