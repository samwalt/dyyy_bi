class CreateHkCustomerStatistics < ActiveRecord::Migration[5.2]
  def change
    create_table :hk_customer_statistics, {id: false} do |t|
      t.string :store_id, :limit=>32, :comment=>'门店id'
      t.date :updated_at
      t.string :k, :limit=>10, :comment=>'数据名'
      t.column :v, 'smallint', :comment=>'数据值'
      t.column :user_type, 'tinyint', :comment=>'用户类型(0:全部1:会员 2:员工 3惯偷 4:回头客5:普通顾客)'
      t.string :t, :limit=>10, :comment=>'数据类型'
    end
    add_index :hk_customer_statistics, [:store_id, :updated_at, :k, :user_type, :t], unique: true, :name=>'idx_uniq'
  end
end
