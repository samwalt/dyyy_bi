class CreateHpItem < ActiveRecord::Migration[5.2]
  def change
    create_table :hp_item do |t|
      t.integer :store_id, :comment=>'门店id'
      t.integer :device_id, :comment=>'货架id'
      t.integer :cell_id, :comment=>'商品标定的区域编号'
      t.integer :sku_id, :comment=>'商品的sku id'
      t.string :item_name, :limit=>50, :comment=>'商品名称'
      t.datetime :updated_at
    end
  end
end
