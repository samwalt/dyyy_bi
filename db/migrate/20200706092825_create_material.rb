class CreateMaterial < ActiveRecord::Migration[5.2]
  def change
    create_table :material do |t|
      t.string :original_name, :limit=>50, :comment=>'原文件名'
      t.string :path, :limit=>256, :comment=>'oss的路径'
      t.column :file_type, 'tinyint', :comment=>'1 图片；2 视频；3 二维码图片'
      t.timestamps
    end
  end
end
