class CreateOrderItem < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_order_item, {id: false} do |t|
      t.string :deptno, :limit=>100, null: false, :comment=>'门店id'
      t.string :orderid, :limit=>300, null: false, :comment=>'销售单号'
      t.string :orderdetailid, :limit=>300, null: false, :comment=>'销售明细流水号'
      t.string :artiid, :limit=>300, null: false, :comment=>'商品id'
      t.decimal :saleqty, :precision=>16, :scale=>2, :comment=>'销售数量'
      t.decimal :saleprice, :precision=>16, :scale=>4, :comment=>'销售单价'
      t.decimal :saleamt, :precision=>16, :scale=>2, :comment=>'销售金额'
      t.datetime :createtime, null: false, :comment=>'交易时间'
      t.integer :status, null: false, default: 0
      t.string :batchcode, :limit=>100, :comment=>'批号'
      t.date :validdate, :comment=>'有效期'
    end
  end
end
