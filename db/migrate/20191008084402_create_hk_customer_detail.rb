class CreateHkCustomerDetail < ActiveRecord::Migration[5.2]
  def change
    create_table :hk_customer_detail, {id: false} do |t|
      t.string :tenant_id, :limit=>64, :comment=>'租户id'
      t.string :store_id, :limit=>32, :comment=>'门店id'
      t.string :store_name, :limit=>64, :comment=>'门店名称'
      t.string :device_serial, :limit=>64, :comment=>'设备序列号'
      t.column :channel_no, 'smallint', :comment=>'通道号'
      t.string :resource_name, :limit=>64, :comment=>'资源名称'
      t.column :user_type, 'tinyint', :comment=>'用户类型(0:全部1:会员 2:员工 3惯偷 4:回头客5:普通顾客)'
      t.string :face_token, :limit=>64, :comment=>'人脸图token'
      t.string :face_url, :limit=>255, :comment=>'人脸图url'
      t.string :standard_token, :limit=>64, :comment=>'标准图token'
      t.string :standard_url, :limit=>255, :comment=>'标准图url'
      t.string :bg_url, :limit=>255, :comment=>'背景图url'
      t.string :similarity, :limit=>32, :comment=>'相似度'
      t.column :sex, 'tinyint'
      t.column :age, 'tinyint'
      t.column :glasses, 'tinyint', :comment=>'是否戴眼镜0:无眼镜 1:有眼镜'
      t.column :face_time, 'bigint', :comment=>'人脸图时间'
      t.datetime :created_at
    end
    add_index :hk_customer_detail, [:device_serial, :face_time], unique: true
  end
end
