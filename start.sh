if [ -z $1 ]; then
    echo "缺少参数，将以开发模式启动。生产模式参数：production  测试模式参数：test"
    puma -e development 1>>access.log 2>>access.log &
else
    puma -e $1 1>>access.log 2>>access.log &
fi