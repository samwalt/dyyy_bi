# 海康摄像头客流 按天
get '/hikvision/passenger_flow/day' do
    $logger.info("/hikvision/passenger_flow/day params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    start_date = params['start_date']
    stop_date = start_date
    stop_date = params['stop_date'] if !params['stop_date'].nil?
    device_serial = params['device_serial']

    start = DateHelper.str_to_long(start_date)
    stop = DateHelper.str_to_long(stop_date) + 3600 * 24 * 1000

    detail = HkCustomerDetail.select("FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') d, count(distinct standard_token) num")
        .where('face_time >= ? and face_time < ? and user_type in (?)', start, stop, [1,3,4,5]).device(device_serial)
        .group("FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d')")

    sum = detail.map {|d| d['num']}.sum
    h = {}
    h['sum'] = sum
    h['detail'] = detail
    [200, h.to_json]
end

# 海康摄像头客流 按小时
get '/hikvision/passenger_flow/hour' do
    $logger.info("/hikvision/passenger_flow/hour params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    start_date = params['start_date']
    stop_date = start_date
    stop_date = params['stop_date'] if !params['stop_date'].nil?
    device_serial = params['device_serial']

    start = DateHelper.str_to_long(start_date)
    stop = DateHelper.str_to_long(stop_date) + 3600 * 24 * 1000

    data = HkCustomerDetail.select('FROM_UNIXTIME(face_time / 1000, \'%H\') AS chour, count(distinct standard_token) c')
        .where('face_time >= ? and face_time < ? and user_type in (?)', start, stop, [1,3,4,5])
        .device(device_serial).group('chour')

    h = {}
    data.each do |d|
        h[d['chour']] = d['c']
    end
    [200, h.to_json]
end

# 海康摄像头客流 按区域
get '/hikvision/passenger_flow/region' do
    $logger.info("/hikvision/passenger_flow/hour params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    start_date = params['start_date']
    stop_date = start_date
    stop_date = params['stop_date'] if !params['stop_date'].nil?
    device_serial = params['device_serial']

    start = DateHelper.str_to_long(start_date)
    stop = DateHelper.str_to_long(stop_date) + 3600 * 24 * 1000

    data = HkCustomerDetail.connection.select_all("SELECT
	COUNT(DISTINCT standard_token) customers, 
	CASE 
    WHEN device_serial = 'D62417303'
    OR device_serial = 'D62417307' THEN
    '美妆'
    WHEN device_serial = 'D62417310'
    OR device_serial = 'D62417305' or device_serial = 'D62417309' or device_serial = 'D62417311'
    THEN
    '保健品'
    WHEN device_serial = 'D62417304' THEN
    '药品'
    else '其它'
    END
    AS region
    FROM
        hk_customer_detail where face_time >= #{start} and face_time < #{stop}
    GROUP BY
    region")
    
    h = {}
    data.each do |d|
        h[d['region']] = d['customers']
    end
    [200, h.to_json]
end

# 海康客群 - 性别
get '/hikvision/customer/sex' do 
    $logger.info("/hikvision/customer/sex params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    return [400, '输入客群类型'] if params['user_type'].nil?

    cs = customer_statistics(params, '性别')
    [200, cs.to_json]
end

# 海康客群 - 年龄段
get '/hikvision/customer/age' do 
    $logger.info("/hikvision/customer/age params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    return [400, '输入客群类型'] if params['user_type'].nil?

    cs = customer_statistics(params, '年龄段')
    [200, cs.to_json]
end

# 海康客群 - 时间段
get '/hikvision/customer/time' do 
    $logger.info("/hikvision/customer/time params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    return [400, '输入客群类型'] if params['user_type'].nil?

    cs = customer_statistics(params, '时间段')
    [200, cs.to_json]
end

# 海康客群 - 到店次数
get '/hikvision/customer/frequency' do 
    $logger.info("/hikvision/customer/freqncecy params: #{params}")
    return [400, '输入开始日期'] if params['start_date'].nil?
    return [400, '输入客群类型'] if params['user_type'].nil?

    cs = customer_statistics(params, '次数')
    [200, cs.to_json]
end

# 热力
get '/hikvision/heat' do
    [200, $container['heats'].to_json]
end

def customer_statistics(params, t)
    start_date = params['start_date']
    stop_date = start_date
    stop_date = params['stop_date'] if !params['stop_date'].nil?
    user_type = params['user_type'].to_i

    cs = HkCustomerStatistics.select('k, sum(v) v').where('updated_at >= ? and updated_at <= ? and user_type = ? and t = ?', start_date, stop_date, user_type, t).group('k')
end

# 海康摄像头列表
get '/hikvision/device' do
    devices = HkDevice.all
    [200, devices.to_json]    
end

post '/hikvision/sync_face_detail' do 
    date = params['date']
    SyncHik.sync_face_details(date)
end