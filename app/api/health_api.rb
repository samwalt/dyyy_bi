
get '/health/user' do
    users = CarelinkerUser.all
    [200, users.to_json]
end

get '/health/measurement' do
    measurements = CarelinkerMeasurement.all
    [200, measurements.to_json]
end

get '/health' do
    api = params['api']
    begin_time = params['begin_time']
    end_time = params['end_time']
    data = SyncCarelinker.call_api(api, begin_time, end_time)
    [200, data.to_json]
end