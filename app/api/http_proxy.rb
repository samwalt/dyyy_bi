DYYY_HOST = 'http://10.10.14.118:9292'

get '/medicare/api/*' do
    real_path = request.path[13..-1]
    query_string = request.query_string
    url = DYYY_HOST + real_path + '?' + query_string
    body = HTTP.timeout(10).get(url).body
    [200, body]
end