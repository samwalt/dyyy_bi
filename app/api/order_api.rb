get '/order/statistics' do 
    $logger.info("/order/statistics params: #{params}")
    return [400, 'store_id 门店id必填'] if params['store_id'].nil?
    return [400, '输入开始日期'] if params['start_date'].nil?

    start_date = params['start_date']
    stop_date = params['stop_date'].nil? ? start_date : params['stop_date']
    page_size = params['page_size'].nil? ? 2000 : params['page_size']

    orders = DyyyErp.query_order(params, start_date, stop_date, 1, page_size)

    h = {}
    h = orders.group_by{|o| o['tranDate'][0...10]} if !orders.nil?
    a = []
    h.each_key do |day|
        day_orders = h[day]
        total_sales = day_orders.reduce(0) {|sum, o| sum + o['saleAmt']}.round 2
        average_sales = (total_sales / day_orders.size).round 2
        member_sales = day_orders.count {|o| o['isMember'] == 1}
        r = {}
        r['日期'] = day
        r['销售额'] = total_sales
        r['客单价'] = average_sales
        r['总单数'] = day_orders.size
        r['会员单数'] = member_sales
        r['非会员单数'] = day_orders.size - member_sales
        a << r
    end
    a.sort! {|i, j| i['日期'] <=> j['日期']}
    [200, JSON.generate(a)]
end

# 订单列表
get '/order' do 
    return [400, 'store_id 门店id必填'] if params['store_id'].nil?
    return [400, 'date 日期必填'] if params['date'].nil?

    stop_date = start_date = params['date']
    page_size = params['page_size'].nil? ? 500 : params['page_size']
    page_no = params['page_no'].nil? ? 1 : params['page_no']

    orders = DyyyErp.query_order(params, start_date, stop_date, page_no, page_size)
    results = []
    results = orders.map {|o| {'deptno'=>o['deptId'], 'orderid'=>o['tranId'], 'saleamt'=>o['saleAmt'], 'createtime'=>o['tranDate'], 'posid'=>o['posId'], 'memcardno'=>o['memCardNo']}} if !orders.nil?
    [200, results.to_json]
end

# 订单详情
get '/order/:id' do 
    id = params['id']
    orders = DyyyErp.query_detail(id)
    results = orders.map {|o| {'orderid'=>o['tranId'], 'saleqty'=>o['artiQty'], 'saleprice'=>o['salePrice'], 'saleamt'=>o['saleAmt']}}
    [200, results.to_json]
end