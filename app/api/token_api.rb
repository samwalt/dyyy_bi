post '/api/signin' do
    params = JSON.parse(request.body.read)
    return [400, 'client_id必填'] if params['client_id'].nil?
    return [400, 'store_id必填'] if params['store_id'].nil?
    return [400, 'timestamp必填'] if params['timestamp'].nil?
    return [400, 'sign必填'] if params['sign'].nil?

    client_sign = params.delete('sign')
    sign = Sign.sign(params, $container['client_secret'])
    raise BusinessException.new('签名不正确') if !sign.eql?(client_sign)

    store_id = params['store_id']
    now = Time.now
    t = Token.find_by(store_id: store_id, valid_date: now, from: 'myself')
    token = nil
    if t.nil?
        token = Digest::MD5.hexdigest(now.to_s)
        Token.create!(store_id: store_id, token: token, valid_date: now, from: 'myself')
    else
        token = t.token
    end
    return [200, token]
end