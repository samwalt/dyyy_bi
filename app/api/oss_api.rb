require 'aliyun/sts'

Role_Arn = 'acs:ram::1762301642101029:role/dyyy-ram-oss'
client = Aliyun::STS::Client.new(
    :access_key_id => $container['oss_access_key'],
    :access_key_secret => $container['oss_access_secret'])

post '/sts/token' do

    policy = Aliyun::STS::Policy.new
    policy.allow(
      ['oss:Get*', 'oss:PutObject'],
      ['acs:oss:*:*:bldyyy', 'acs:oss:*:*:bldyyy/*'])

    token = client.assume_role(Role_Arn, 'dyyy', policy, 3600)
  
    h = {}
    h['access_key_id'] = token.access_key_id
    h['access_key_secret'] = token.access_key_secret
    h['security_token'] = token.security_token
    h['expiration'] = token.expiration.getlocal
    [200, h.to_json]
end
