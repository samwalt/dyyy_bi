# 人加货架
get '/humanplus/shelf' do 
    shelves = LocalHpShelf.select('device_id, shelf_name')
    [200, shelves.to_json]
end

# 人加货架转化
get '/humanplus/shelf/:id/conversion' do 
    $logger.info("/humanplus/shelf/:id/conversion params: #{params}")
    id = params['id'].to_i
    return [400, '输入开始日期'] if params['start_date'].nil?
    start_date = params['start_date']
    stop_date = start_date
    stop_date = params['stop_date'] if !params['stop_date'].nil?

    conversions = LocalHpShelfConversion.select('sum(pass_people) pass_people, sum(stay_people) stay_people, sum(interact_people) interact_people').where('device_id = ? and updated_at >= ? and updated_at <= ?', id, start_date, stop_date)
    [200, conversions.to_json]
end

# 人加商品转化
get '/humanplus/item' do 
    date = params['date'] + ' 00:00:00'
    conversions = LocalHpItemConversion.select('hp_item_conversion.interact_people, hp_item_conversion.interact_count , hp_item.item_name')
    .joins("join hp_item on hp_item_conversion.store_id = hp_item.store_id and hp_item_conversion.device_id = hp_item.device_id and hp_item_conversion.cell_id = hp_item.cell_id and hp_item_conversion.sku_id = hp_item.sku_id")
    .where("hp_item_conversion.updated_at = ?", date)
    [200, conversions.to_json]
end

# 热门商品
get '/humanplus/item/top' do
    data = LocalHpItemConversion.connection.select_all("SELECT
	hp_item.item_name 商品名,
	p_num 人数, a_num 次数
FROM
	(
		SELECT
			sku_id,
            device_id,
			sum(interact_people) AS p_num,
			sum(interact_count) AS a_num  
		FROM
			hp_item_conversion
		WHERE
			updated_at >= DATE_SUB(curdate(), INTERVAL 2 DAY)
		GROUP BY
			sku_id, device_id
	) inter_tbl
JOIN hp_item ON hp_item.sku_id = inter_tbl.sku_id and hp_item.device_id=inter_tbl.device_id
ORDER BY
    a_num DESC")
    [200, data.to_json]
end