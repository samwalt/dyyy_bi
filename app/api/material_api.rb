require 'aliyun/oss'

BLDYYY = 'bldyyy'
ALIYUN_HOST = "https://#{BLDYYY}.oss-cn-shanghai.aliyuncs.com"

# 保存媒体文件信息
post '/material' do
    input = request.body.read
    $logger.info("POST /material 参数#{input.force_encoding('utf-8')}")
    params = JSON.parse(input)
    return [400, 'original_name必填'] if params['original_name'].nil?
    return [400, 'path必填'] if params['path'].nil?
    return [400, 'type必填'] if params['type'].nil?

    original_name = params['original_name']
    path = params['path']
    type = params['type']
    m = Material.create!(original_name: original_name, path: path, file_type: type)
    m.path = ALIYUN_HOST + '/' + m.path
    [200, m.to_json]
end

get '/material' do
    return [400, 'type必填'] if params['type'].nil?
    if params['store_id'].nil?
        materials = Material.where('file_type = ?', params['type'])
    else
        materials = Material.select('material.id, material.file_type, original_name, path, material_store.id as rel_id, material_store.enabled')
        .joins('join material_store on material_store.material_id = material.id')
        .where('material.file_type = ? and material_store.store_id = ?', params['type'], params['store_id'])
    end
    materials.each do |m|
        m.path = ALIYUN_HOST + '/' + m.path
    end
    [200, materials.to_json]
end

delete '/material/:id' do
    id = params['id'].to_i
    ms = MaterialStore.find_by(material_id: id)
    raise BusinessException.new('文件已被店铺使用，无法删除') if !ms.nil?

    m = Material.find(id)
    client = Aliyun::OSS::Client.new(endpoint: $container['oss_host'], access_key_id: $container['oss_access_key'], access_key_secret: $container['oss_access_secret'])
    bucket = client.get_bucket(BLDYYY)
    bucket.delete_object(m.path)
    m.destroy
    [200, m.to_json]
end

post '/material/store' do
    input = request.body.read
    $logger.info("POST /material/store 参数#{input}")
    params = JSON.parse(input)
    return [400, 'materials必填'] if params['materials'].nil? || params['materials'].size == 0
    return [400, 'stores必填'] if params['stores'].nil? || params['stores'].size == 0

    materials = params['materials']
    stores = params['stores']

    materials.each do |m|
        stores.each do |s|
            ms = MaterialStore.find_by(material_id: m, store_id: s)
            material = Material.find(m)
            MaterialStore.create!(material_id: m, store_id: s, file_type: material.file_type) if ms.nil?
        end
    end
    200
end

put '/material/store/:id' do
    id = params['id']
    input = request.body.read
    $logger.info("PUT /material/store/#{id} 参数#{input}")
    json = JSON.parse(input)
    return [400, 'enabled必填'] if json['enabled'].nil?

    enabled = json['enabled']
    ms = MaterialStore.find(id)
    ms.enabled = enabled

    MaterialStore.transaction do
        if enabled == 1
            if ms.file_type == 1
                records = MaterialStore.where('store_id = ? and file_type = 2 and enabled = 1', ms.store_id)
                records.each do |r|
                    r.enabled = 0
                    r.save
                end
            end
            if ms.file_type == 2
                records = MaterialStore.where('store_id = ? and file_type = 1 and enabled = 1', ms.store_id)
                records.each do |r|
                    r.enabled = 0
                    r.save
                end
            end
        end
        ms.save
    end
    200
end

delete '/material/store/:id' do
    id = params['id'].to_i
    ms = MaterialStore.find(id)
    ms.destroy
    [200, ms.to_json]
end

get '/api/material' do
    $logger.info("/api/material params: #{params.to_s}")
    return [400, 'client_id必填'] if params['client_id'].nil?
    return [400, 'store_id必填'] if params['store_id'].nil?
    return [400, 'timestamp必填'] if params['timestamp'].nil?
    return [400, 'sign必填'] if params['sign'].nil?

    Sign.auth(params)
    materials = Material.select('material.id, material.path, material.file_type, material_store.enabled')
    .joins('join material_store on material_store.material_id = material.id')
    .joins('join store on store.id = material_store.store_id')
    .where('store.code = ?', params['store_id'])

    materials.each do |m|
        m.path = ALIYUN_HOST + '/' + m.path
    end
    [200, materials.to_json]
end