post '/face/standard' do
    mobile = params['mobile']
    overwrite = params['overwrite']
    $logger.info('保存人脸标准图片和手机号: ' + mobile)
    image = BlFaceImage.find_by(mobile: mobile)
    r = 0
    if !image || '1'.eql?(overwrite)
        file = params['file']
        tempfile = file['tempfile']

        file_name = mobile + '.jpg'
        face_image_path = $container['face_image_path']

        Dir.mkdir(face_image_path) unless Dir.exist?(face_image_path)

        file_path = face_image_path + '/' + file_name

        File.open(file_path, 'wb') do |f|
            f.write(tempfile.read)
        end

        if !image
            BlFaceImage.create!(image_path: '/'+file_name, mobile: mobile)
        else
            image.updated_at = Time.now
            image.save
        end
        r = 1
    end
    [200, r.to_s]
end

get '/face/standard' do
    $logger.info('标准人脸图片列表')
    images = BlFaceImage.all
    result = []
    images.each do |i|
        h = {}
        h['mobile'] = i.mobile
        h['image_url'] = "http://#{request.host}:#{request.port}" + i.image_path
        result << h
    end
    [200, result.to_json]
end

put '/face/camera' do
    input = JSON.parse(request.body.read)
    $logger.info('保存摄像头图片识别结果' + input.to_s)

    device_serial = input['device_serial']
    face_time = input['face_time']
    mobile = input['mobile']

    d = HkCustomerDetail.where('device_serial = ? and face_time = ?', device_serial, face_time).update_all(mobile: mobile)
    [200, d.to_json]
end

get '/face/camera' do
    $logger.info('获取摄像头抓拍图片' + params.to_s)
    face_time_start = params['face_time_start'].to_i
    face_time_end = params['face_time_end'].to_i

    d = HkCustomerDetail.select('mobile, face_url, device_serial, face_time').where('face_time >= ? and face_time <= ? and user_type != 3', face_time_start, face_time_end)
    [200, d.to_json]
end

get '/face' do
    $logger.info('展示和搜索已识别和未识别的人物信息' + params.to_s)
    page_no = params['page_no'].to_i
    page_size = params['page_size'].to_i
    mobile = params['mobile']
    date = params['date']
    sex = params['sex']

    offset = (page_no - 1) * page_size

    total_no = HkCustomerDetail.date(date).mobile(mobile).sex(sex).count
    result = HkCustomerDetail.select('resource_name, face_url, sex, age, mobile, face_time')
        .date(date).mobile(mobile).sex(sex).order(face_time: :desc).limit(page_size).offset(offset)

    total_page = (total_no - 1) / page_size + 1
    h = {}
    h['data'] = result
    h['total_page'] = total_page
    [200, h.to_json]
end