class HkCustomerDetail < LocalDb
    self.table_name = 'hk_customer_detail'

    scope :device, ->(device_serial) {
        if !device_serial.nil?
            where('device_serial = ?', device_serial)
        else
            where('device_serial in (?)', %w{D62417304 D62417303 D62417307})
        end
    }

    scope :date, ->(date) {
        if !date.nil?
            d = date.split('-')
            t = Time.local(d[0], d[1], d[2]).to_i
            start = t * 1000
            stop = start + 24 * 3600 * 1000
            where('face_time >= ? and face_time < ?', start, stop)
        end
    }

    scope :mobile, ->(mobile) {
        where('mobile = ?', mobile) if !mobile.nil?
    }

    scope :sex, ->(sex) {
        where('sex = ?', sex) if !sex.nil?
    }
end