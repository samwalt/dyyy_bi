class Sign
    def self.sign(params, token)
        keys = params.keys.sort
        
        s = ''
        keys.each do |k|
            s << "#{k}=#{params[k]}&"
        end
        s << "key=#{token}"
        $logger.info('签名前字符串: ' + s)
        s = Digest::MD5.hexdigest(s)
        $logger.info('签名后字符串: ' + s)
        s
    end

    def self.auth(params)
        t = Token.find_by(store_id: params['store_id'], valid_date: Time.now, from: 'myself')
        raise BusinessException.new('未签到') if t.nil?
    
        client_sign = params.delete('sign')
        sign = sign(params, t.token)
        raise BusinessException.new('签名不正确') if !sign.eql?(client_sign)
    end
end