require 'openssl'
require 'base64'

class SyncCarelinker
    HOST = 'https://platform.carelinker.com' 
    Carelinker = 'carelinker'
    MeasurementRecords = 'MeasurementRecords'
    UserProfiles = 'UserProfiles'
    ONE_DAY = 24 * 60 * 60

    def self.sync
        sync_user
        sync_measurement
    end

    private

    def self.call_api(api, begin_time, end_time)
        url = HOST + '/tp/api/thirdparty/'
        now = Time.now.strftime('%Y%m%d%H%M%S')
        pre_sign = "#{api}?state=interval&begin=#{begin_time}&end=#{end_time}&accessId=#{$container['carelinker_id']}&timestamp=#{now}"
        signature = sign(pre_sign)
        url << pre_sign << "&signature=#{signature}"
        $logger.info('请求蓝信康 ' + url)
        body = HTTP.timeout(10).get(url).body
        body_json = JSON.parse(body)
        if body_json['error'] != 200
            $logger.error('蓝信康返回错误 ' + body)
            return
        end
        body_json
    end

    def self.sync_user
        r = SynchronizeRecord.find_by(source: Carelinker, api: UserProfiles)
        begin_time, end_time = time_gap(r)

        body_json = call_api(UserProfiles, begin_time, end_time)
        CarelinkerUser.transaction do 
            if !body_json.nil?
                users = body_json['userProfiles']
                $logger.info("蓝信康用户数据#{users.size}条")
                users.each do |u|
                    user = CarelinkerUser.find_by(user_name: u['userName'])
                    if user.nil?
                        CarelinkerUser.create!(branch_storeid: u['branchStoreId'], user_name: u['userName'], ms_card: u['msCard'], birthday: u['birthday'], weight: u['weight'], height: u['height'], gender: u['gender'], name: u['name'])
                    else
                        user.birthday = u['birthday']
                        user.weight = u['weight']
                        user.height = u['height']
                        user.gender = u['gender']
                        user.name = u['name']
                        user.ms_card = u['msCard']
                        user.save
                    end
                end
                if r.nil?
                    SynchronizeRecord.create!(source: Carelinker, api: UserProfiles, sync_time: end_time)
                else
                    r.sync_time = end_time
                    r.save!
                end
            end
        end
    end

    def self.sync_measurement
        r = SynchronizeRecord.find_by(source: Carelinker, api: MeasurementRecords)
        begin_time, end_time = time_gap(r)

        body_json = call_api(MeasurementRecords, begin_time, end_time)
        CarelinkerMeasurement.transaction do 
            if !body_json.nil?
                measurements = body_json['Measurements']
                $logger.info("蓝信康测量数据#{measurements.size}条")
                measurements.each do |m|
                    CarelinkerMeasurement.create!(branch_storeid: m['branchStoreId'], user_name: m['userName'], ms_card: m['msCard'], acquisition_time: m['acquisitionTime'], tc: m['tc'], tg: m['tg'], hdl: m['hdl'], ldl: m['ldl'], systolic: m['systolic'], diastolic: m['diastolic'], heart_rate: m['heartRate'], channel: m['channel'], bloodsugar: m['bloodsugar'], stage: m['stage'], measurement_id: m['id'])
                end
                if r.nil?
                    SynchronizeRecord.create!(source: Carelinker, api: MeasurementRecords, sync_time: end_time)
                else
                    r.sync_time = end_time
                    r.save!
                end
            end
        end
    end

    def self.sign(pre_sign)
        signature = Base64.encode64(OpenSSL::HMAC.digest('sha1', $container['carelinker_secret'], pre_sign))
        signature[0...-1]
    end

    private 
    
    def self.time_gap(r)
        t = Time.now
        if r.nil?
            b = Time.new(t.year, t.month, t.day, 0, 0, 0)
        else
            b = r.sync_time
            t = b + ONE_DAY if t - b > ONE_DAY
        end

        begin_time = b.strftime('%Y%m%d%H%M%S')
        end_time = t.strftime('%Y%m%d%H%M%S')
        return begin_time, end_time
    end
end