
ActiveSupport::Notifications.subscribe('sql.active_record') do |name, start, finish, id, payload|
    duration = finish - start

    if duration >= 1
      $logger.warn("Slow query: #{payload[:sql]}, duration: #{duration}")
    end
end