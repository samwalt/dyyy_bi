class DyyyErp

    def self.query_order(params, start_date, stop_date, page_no, page_size)
        url = $container['dyyy_erp_host'] + '/lserp/service/S_OD_32'
        dept_id = params['store_id']
        data = {}
        data['depts'] = dept_id
        data['pageNum'] = page_no
        data['pageSize'] = page_size
        data['startDate'] = start_date + ' 00:00:00'
        data['endDate'] = stop_date + ' 23:59:59'
        data['bussType'] = params['buss_type'] if !params['buss_type'].nil?
        data['posId'] = params['pos_id'] if !params['pos_id'].nil?

        doc = {}
        doc['deptId'] = dept_id
        doc['empId'] = '2070939'
        t = Token.find_by(store_id: dept_id, from: 'erp')
        doc['token'] = t.token
        h = {}
        h['data'] = data
        h['doc'] = doc

        $logger.info('第一医药erp查询订单请求参数: ' + JSON.generate(h))
        t = Time.now
        begin
            body = HTTP.timeout(10).post(url, :json=>h).body
        rescue => e
            $logger.error("第一医药erp查询订单异常：#{e}")
            return
        end
        $logger.info("第一医药erp查询订单响应时间#{Time.now - t}")
        body_json = JSON.parse(body)
        $logger.info("第一医药erp查询订单返回#{body_json['orderCount']}条订单")
        body_json['result']
    end

    def self.query_detail(order_id)
        url = $container['dyyy_erp_host'] + '/lserp/service/S_OD_34'
        data = {}
        data['tranId'] = order_id
        doc = {}
        dept_id = '15032'
        doc['deptId'] = dept_id
        doc['empId'] = '2070939'
        t = Token.find_by(store_id: dept_id, from: 'erp')
        doc['token'] = t.token
        h = {}
        h['data'] = data
        h['doc'] = doc

        $logger.info('第一医药erp查询订单详情请求参数: ' + JSON.generate(h))
        begin
            body = HTTP.timeout(10).post(url, :json=>h).body
        rescue => e
            $logger.error("第一医药erp查询订单详情异常：#{e}")
            return
        end
        body_json = JSON.parse(body)
        orders = body_json['result']['dtlList']
        $logger.info('第一医药erp查询订单详情返回: ' + JSON.generate(orders))
        orders
    end

    def self.signin
        url = $container['dyyy_erp_host'] + '/lserp/service/S_OD_SYS_03'
        data = {}
        data['deptid'] = '15032'
        data['empid'] = '2070939'
        h = {}
        h['data'] = data

        $logger.info("第一医药erp签到请求参数：" + JSON.generate(h))
        begin
            body = HTTP.timeout(10).post(url, :json=>h).body
            body_json = JSON.parse(body)
            $logger.info('第一医药erp签到返回: ' + body)
        rescue => e
            $logger.error("第一医药erp签到异常：#{e}")
            return
        end
        t = Token.find_by(store_id: '15032', from: 'erp')
        if t.nil?
            Token.create(store_id: '15032', token: body_json['result']['token'], valid_date: Time.now, from: 'erp')
        else
            t.valid_date = Time.now
            t.token = body_json['result']['token']
            t.save
        end
        $logger.info('第一医药erp签到成功')
    end
end