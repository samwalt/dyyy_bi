class SyncHumanPlus
    class HumanPlusDb < ActiveRecord::Base
        self.abstract_class = true
    end
    class HpItem < HumanPlusDb
        self.table_name = '百联-商品信息表'
    end
    class HpItemConversion < HumanPlusDb
        self.table_name = '百联-商品转化表'
    end
    class HpShelf < HumanPlusDb
        self.table_name = '百联-货架信息表'
    end
    class HpShelfConversion < HumanPlusDb
        self.table_name = '百联-货架转化表'
    end

    def self.sync
        pool = HumanPlusDb.class_eval do
            db = YAML::load(IO.read(File.expand_path('../../../db/config.yml', __FILE__)))
            establish_connection(db['humanplus'][ENV['RACK_ENV']])
        end

        sync_shelf
        sync_item
        sync_shelf_conversion
        sync_item_conversion

        pool.disconnect
    end

    private

    def self.sync_shelf
        updated_at = LocalHpShelf.maximum('updated_at')
        records = nil
        if updated_at.nil?
            records = HpShelf.where('StoreID != 1')
            LocalHpShelf.transaction do
                records.each do |r|
                    record = LocalHpShelf.new
                    record.store_id = r.StoreID
                    record.device_id = r.DeviceID
                    record.region = r.地区
                    record.province = r.省份
                    record.city = r.城市
                    record.store_name = r.店铺
                    record.shelf_name = r.货架
                    record.updated_at = r.更新时间
                    record.save
                end
            end
        else
            records = HpShelf.where('更新时间 > ? and StoreID != 1', updated_at)
            LocalHpShelf.transaction do
                records.each do |r|
                    record = LocalHpShelf.where(store_id: r.StoreID, device_id: r.DeviceID).take
                    if record.nil?
                        record = LocalHpShelf.new
                        record.store_id = r.StoreID
                        record.device_id = r.DeviceID
                    end
                    record.region = r.地区
                    record.province = r.省份
                    record.city = r.城市
                    record.store_name = r.店铺
                    record.shelf_name = r.货架
                    record.updated_at = r.更新时间
                    record.save
                end
            end
        end
        if records
            $logger.info("更新#{records.size}条货架信息")
        else
            $logger.info("更新0条货架信息")
        end
    end
    
    def self.sync_item
        return if !need_sync?
        updated_at = LocalHpItem.maximum('updated_at')
        records = nil
        if updated_at.nil?
            records = HpItem.all
            LocalHpItem.transaction do
                records.each do |r|
                    record = LocalHpItem.new
                    record.store_id = r.StoreID
                    record.device_id = r.DeviceID
                    record.cell_id = r.CellID
                    record.sku_id = r.SKUID
                    record.item_name = r.商品名称
                    record.updated_at = r.更新时间
                    record.save
                end
            end
        else
            records = HpItem.where('更新时间 > ?', updated_at)
            LocalHpItem.transaction do
                records.each do |r|
                    record = LocalHpItem.where(store_id: r.StoreID, device_id: r.DeviceID, cell_id: r.CellID, sku_id: r.SKUID).take
                    if record.nil?
                        record = LocalHpItem.new
                        record.store_id = r.StoreID
                        record.device_id = r.DeviceID
                    end
                    record.cell_id = r.CellID
                    record.sku_id = r.SKUID
                    record.item_name = r.商品名称
                    record.updated_at = r.更新时间
                    record.save
                end
            end
        end
        if records
            $logger.info("更新#{records.size}条商品信息")
        else
            $logger.info("更新0条商品信息")
        end

    end

    def self.sync_shelf_conversion
        return if !need_sync?
        updated_at = LocalHpShelfConversion.maximum('updated_at')
        conversions = nil
        if updated_at.nil?
            conversions = HpShelfConversion.all
        else
            conversions = HpShelfConversion.where('日期 >= ?', updated_at)
        end
        LocalHpShelfConversion.transaction do
            conversions.each do |c|
                t = LocalHpShelfConversion.find_by(store_id: c.StoreID, device_id: c.DeviceID, updated_at: c.日期)
                if t.nil?
                    t = LocalHpShelfConversion.new
                    t.store_id = c.StoreID
                    t.device_id = c.DeviceID
                end
                t.pass_people = c.过道人次
                t.stay_people = c.驻足人次
                t.interact_people = c.交互人次
                t.updated_at = c.日期
                t.save!
            end
        end
        if conversions
            $logger.info("更新#{conversions.size}条货架转化信息")
        else
            $logger.info("更新0条货架转化信息")
        end
    end

    def self.sync_item_conversion
        return if !need_sync?
        updated_at = LocalHpItemConversion.maximum('updated_at')
        conversions = nil
        if updated_at.nil?
            conversions = HpItemConversion.all
        else
            conversions = HpItemConversion.where('日期 >= ?', updated_at)
        end
        LocalHpItemConversion.transaction do
            conversions.each do |c|
                t = LocalHpItemConversion.find_by(store_id: c.StoreID, device_id: c.DeviceID, cell_id: c.CellID, sku_id: c.SKUID, updated_at: c.日期)
                if t.nil?
                    t = LocalHpItemConversion.new
                    t.store_id = c.StoreID
                    t.device_id = c.DeviceID
                    t.cell_id = c.CellID
                    t.sku_id = c.SKUID
                end
                t.interact_people = c.交互人次
                t.interact_count = c.交互次数
                t.updated_at = c.日期
                t.save!
            end
        end
        if conversions
            $logger.info("更新#{conversions.size}条商品转化信息")
        else
            $logger.info("更新0条商品转化信息")
        end

    end

    def self.need_sync?()
        t = Time.now
        hour = t.hour
        # return true if hour >= 7 && hour <= 22
        # false
        true
    end
end