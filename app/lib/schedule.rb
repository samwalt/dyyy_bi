require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new

scheduler.every '1800s', :first_in=>'5s' do
    SyncHumanPlus.sync

    SyncHik.sync_access_token
    SyncHik.sync_device
    SyncHik.sync_face_details(Time.now.strftime('%Y-%m-%d'))
    SyncHik.compute_sex
    SyncHik.compute_age
    SyncHik.compute_time
    SyncHik.compute_frequency
    SyncHik.sync_heat

    SyncCarelinker.sync
end

scheduler.cron '00 00 * * *', :first_in=>'1s' do
    DyyyErp.signin
end
