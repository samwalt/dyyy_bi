
class SyncHik
    HK_HOST = 'https://api2.hik-cloud.com'

    def self.sync_access_token
        url = HK_HOST + '/oauth/token'
        client_id = $container['hk_client_id']
        client_secret = $container['hk_client_secret']
        grant_type = 'client_credentials'
        scope = 'app'

        h = {}
        h['client_id'] = client_id
        h['client_secret'] = client_secret
        h['grant_type'] = grant_type
        h['scope'] = scope
        body = HTTP.post(url, :form=>h).body
        body_json = JSON.parse(body)
        if !body_json['access_token'].nil?
            $container['hk_access_token'] = body_json['access_token']
        else
            raise '获取access token发生异常'
        end
        $logger.info('成功获取海康access_token: ' + $container['hk_access_token'])
    end

    def self.sync_face_details(date)
        sync_access_token if $container['hk_access_token'].nil?
        store_ids = get_store
        url = HK_HOST + '/v1/customization/face/details'

        has_next_page = true
        page_no = 1
        page_size = 999

        while has_next_page
            h = {}
            h['pageNo'] = page_no
            h['pageSize'] = page_size
            h['storeId'] = store_ids[0]
            h['dateTime'] = date
            $logger.info("#{url} params: #{h}")
            body = HTTP.auth("Bearer #{$container['hk_access_token']}").get(url, :params=>h).body
            body_json = JSON.parse(body)
            if body_json['code'] != 200
                $logger.error('获取客群明细发生异常' + body_json.to_s)
                raise BusinessException.new('获取客群明细发生异常')
            end
            c = 0
            repli = 0
            rows = body_json['data']['rows']
            HkCustomerDetail.transaction do
                rows.each do |r|
                    d = HkCustomerDetail.new
                    d.tenant_id = r['tenantId']
                    d.store_id = r['storeId']
                    d.store_name = r['storeName']
                    d.device_serial = r['deviceSerial']
                    d.channel_no = r['channelNo']
                    d.resource_name = r['resourceName']
                    d.user_type = r['userType']
                    d.face_token = r['faceToken']
                    d.face_url = r['faceUrl']
                    d.standard_token = r['standardToken']
                    d.standard_url = r['standardUrl']
                    d.bg_url = r['bgUrl']
                    d.similarity = r['similarity']
                    d.sex = r['sex']
                    d.age = r['age']
                    d.glasses = r['glasses']
                    d.face_time = r['faceTime']
                    begin
                        d.save!
                        c += 1
                    rescue ActiveRecord::RecordNotUnique
                        repli += 1
                    end
                end
            end
            $logger.info("保存了#{c}条客群明细，过滤了#{repli}条重复数据")
            has_next_page = body_json['data']['hasNextPage']
            page_no += 1 if has_next_page
            break if c == 0
        end
    end

    def self.sync_heat
        now = Time.now
        hour = now.hour
        return if hour < 10 || hour >= 22

        sync_access_token if $container['hk_access_token'].nil?

        # make_heat_url = "https://api2.hik-cloud.com/v1/customization/heats/actions/generateHeatMapPicture"
        get_heat_url = "https://api2.hik-cloud.com/v1/customization/heats/picture"

        a = []

        h = {}
        %w(fb736d15cbbf41668dd6a56cbf0def23 43f375c0795d4ad2b7d463c41229fafa).each do |c|
            h['channelId'] = c
            # h['startTime'] = Time.new(now.year, now.month, now.day, 8, 0, 0).strftime('%Y-%m-%d %H:%M:%S')
            # h['endTime'] = now.strftime('%Y-%m-%d %H:%M:%S')

            # body = HTTP.auth("Bearer #{$container['hk_access_token']}").post(make_heat_url, :form=>h).body
            # body_json = JSON.parse(body)
            # if body_json['code'] != 200
            #     $logger.error('生成热力图失败: ' + body)
            #     next
            # end

            # h.delete('startTime')
            # h.delete('endTime')

            body = HTTP.auth("Bearer #{$container['hk_access_token']}").get(get_heat_url, :params=>h).body
            body_json = JSON.parse(body)
            if body_json['code'] != 200
                $logger.error('获取热力图失败: ' + body)
            else
                a << body_json['data']['pictureUrl']
            end
        end

        if a.size > 0
            $container['heats'] = a 
            $logger.info("更新了#{a.size}张热力图")
        end
    end

    def self.compute_sex
        HkCustomerDetail.connection.insert("INSERT INTO hk_customer_statistics(store_id, updated_at, k, v, user_type, t) SELECT
        store_id,
        update_at,
        CASE sex
        WHEN 1 THEN
            '男'
        ELSE
            '女'
        END,
        num,
        0,
        '性别'
        FROM
        (
            SELECT
                count(DISTINCT standard_token) AS num,
                store_id,
                sex,
                FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') AS update_at
            FROM
                hk_customer_detail
            WHERE
                FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') = CURDATE()
            AND device_serial IN (
                'D62417307',
                'D62417303',
                'D62417304'
            )
            GROUP BY
                store_id,
                sex,
                update_at
        ) a ON DUPLICATE KEY UPDATE v = num ")
        $logger.info('更新了性别统计数据')
    end

    def self.compute_sex_repeat_customer
        HkCustomerDetail.connection.insert("
            replace into hk_customer_statistics(store_id,updated_at,k,v,user_type,t)
SELECT
repeatTb.store_id,
repeatTb.dt updated_at,
repeatTb.sex k,
COUNT(DISTINCT repeatTb.standard_url) v,
repeatTb.user_type user_type,
'性别'
FROM
(

(SELECT
store_id,
CASE sex
WHEN 1 THEN
    '男'
ELSE
    '女'
END sex,
standard_url,
date_format(
			FROM_UNIXTIME(face_time / 1000),
			'%Y-%m-%d'
		)dt,
user_type
FROM
hk_customer_detail
WHERE user_type = 4
AND date_format(
			FROM_UNIXTIME(face_time / 1000),
			'%Y-%m-%d'
		) = CURDATE()

)
repeatTb,
(SELECT
tb1.store_id,
tb1.standard_url,
tb1.dt
FROM
(SELECT
store_id,
standard_url,
date_format(
			FROM_UNIXTIME(face_time / 1000),
			'%Y-%m-%d'
		)dt,
user_type
FROM
hk_customer_detail
WHERE user_type = 5
AND date_format(
			FROM_UNIXTIME(face_time / 1000),
			'%Y-%m-%d'
		) = CURDATE()
)tb1
,
(
SELECT
store_id,
standard_url,
date_format(
			FROM_UNIXTIME(face_time / 1000),
			'%Y-%m-%d'
		) dt,
user_type
FROM
hk_customer_detail
WHERE user_type = 4
AND date_format(
			FROM_UNIXTIME(face_time / 1000),
			'%Y-%m-%d'
		) = CURDATE()

) tb2
WHERE tb1.dt = tb2.dt
AND tb1.standard_url = tb2.standard_url

)
NotRepeatTb
)
WHERE repeatTb.dt = NotRepeatTb.dt
AND repeatTb.standard_url != NotRepeatTb.standard_url
GROUP BY store_id,updated_at,k,user_type
            ")
        $logger.info('更新了回头客性别统计数据')
    end


    def self.compute_age
        HkCustomerDetail.connection.insert("INSERT INTO hk_customer_statistics (store_id, updated_at, k, v, user_type, t) 
        SELECT
            store_id,
            update_at,
            age,
            num,
            0,
            '年龄段'
        FROM
            (
                SELECT store_id, 
                    count(DISTINCT
                    standard_token) num,
                    CASE
                WHEN age < 14 THEN
                    '少年'
                WHEN 14 <= age
                AND age < 35 THEN
                    '青年'
                WHEN 35 <= age
                AND age < 60 THEN
                    '中年'
                ELSE
                    '老年'
                END AS age,
                FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') AS update_at
            FROM
                hk_customer_detail
            WHERE
                FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') = CURDATE()
            AND device_serial IN (
                'D62417307',
                'D62417303',
                'D62417304'
            )
        GROUP BY
            store_id,
            age,
            update_at
        ) a ON DUPLICATE KEY UPDATE v = num ")
        $logger.info('更新了年龄段统计数据')
    end

    def self.compute_time
         HkCustomerDetail.connection.insert("INSERT INTO hk_customer_statistics (store_id, updated_at, k, v, user_type, t) 
         SELECT
	store_id,
	update_at,
	k_hour,
	num,
	0,
	'时间段'
FROM
	(
		SELECT
			store_id,
			count(DISTINCT standard_token) AS num,
			FROM_UNIXTIME(face_time / 1000, '%H:00') AS k_hour,
			FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') AS update_at
		FROM
			hk_customer_detail
		WHERE
			FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') = CURDATE()
		AND device_serial IN (
			'D62417307',
			'D62417303',
			'D62417304'
		)
		GROUP BY
			store_id,
			update_at,
			k_hour
    ) a ON DUPLICATE KEY UPDATE v = num ")
        $logger.info('更新了时间段统计数据')
    end

    def self.compute_frequency
        HkCustomerDetail.connection.insert("
        REPLACE into hk_customer_statistics(store_id,updated_at,k,v,user_type,t)
        select
        tb1.store_id,
        tb1.dt updated_at,
        CASE WHEN tb1.ft=1 THEN 1
        WHEN tb1.ft = 2 THEN 2
        when tb1.ft = 3 THEN 3
        WHEN tb1.ft = 4 THEN 4
        WHEN tb1.ft = 5 THEN 5
        WHEN tb1.ft >5 THEN  '>5'
        END k,
        COUNT(tb1.standard_url) num,
        tb1.user_type,
        '次数'
        FROM (
            SELECT
            store_id,
            user_type,
            standard_url,
            date_format(FROM_UNIXTIME(face_time / 1000), '%Y-%m-%d') dt,
            count(
                DISTINCT date_format(
                    FROM_UNIXTIME(face_time / 1000),
                    '%Y-%m-%d'
                )
            ) ft
            FROM hk_customer_detail
            WHERE
            FROM_UNIXTIME(face_time / 1000, '%Y-%m-%d') = CURDATE() and 
            user_type != 2
            GROUP BY
            store_id, user_type, standard_url, dt
        ) tb1
        GROUP BY
        store_id,updated_at,k,user_type")
        $logger.info('更新了到店次数统计数据')
    end

    def self.get_store
        url = HK_HOST + '/v1/customization/storeInfo'
        h = {}
        h['pageNo'] = 1
        h['pageSize'] = 99
        $logger.info("#{url} params: #{h}")
        body = HTTP.auth("Bearer #{$container['hk_access_token']}").get(url, :params=>h).body
        body_json = JSON.parse(body)
        if body_json['code'] != 200
            $logger.error('获取门店id发生异常' + body_json.to_s)
            raise BusinessException.new('获取门店id发生异常')
        end
        store_ids = body_json['data']['rows'].map{|s| s['storeId']}
    end

    def self.sync_device
        sync_access_token if $container['hk_access_token'].nil?
        store_ids = get_store

        url = HK_HOST + '/v1/customization/cameraList'
        h = {}
        h['pageNo'] = 1
        h['pageSize'] = 999
        h['storeId'] = store_ids[0]
        $logger.info("#{url} params: #{h}")

        body = HTTP.auth("Bearer #{$container['hk_access_token']}").get(url, :params=>h).body
        body_json = JSON.parse(body)
        if body_json['code'] != 200
            $logger.error('获取设备通道列表发生异常' + body_json.to_s)
            raise BusinessException.new('获取设备通道列表发生异常')
        end
        rows = body_json['data']['rows']
        HkDevice.transaction do
            rows.each do |r|
                device = HkDevice.find_by(device_id: r['deviceId'], channel_no: r['channelNo'])
                if device.nil?
                    HkDevice.create!(device_id: r['deviceId'], device_name: r['deviceName'], device_model: r['deviceModel'], device_serial: r['deviceSerial'], channel_id: r['channelId'], channel_name: r['channelName'], channel_no: r['channelNo'], channel_status: r['channelStatus'], channel_pic_url: r['channelPicUrl'])
                else
                    device.device_id = r['deviceId']
                    device.device_name = r['deviceName']
                    device.device_model = r['deviceModel']
                    device.device_serial = r['deviceSerial']
                    device.channel_id = r['channelId']
                    device.channel_name = r['channelName']
                    device.channel_no = r['channelNo']
                    device.channel_status = r['channelStatus']
                    device.channel_pic_url = r['channelPicUrl']
                    device.save!
                end
            end
        end
        $logger.info("更新了#{rows.size}条设备数据")
    end

    def self.sync_face_statistics
        sync_access_token if $container['hk_access_token'].nil?
        store_ids = get_store

        url = HK_HOST + '/v1/customization/face/statistics'
        h = {}
        h['storeId'] = store_ids[0]
        h['dateTime'] = (Time.now-3600*24).strftime('%Y-%m-%d')
        $logger.info("#{url} params: #{h}")
        body = HTTP.auth("Bearer #{$container['hk_access_token']}").get(url, :params=>h).body
        body_json = JSON.parse(body)
        raise '获取单日客群属性统计发生异常' if body_json['code'] != 200

        cs = []
        data = body_json['data']

        data.each do |d|
            user_type = d['userType']
            sex_data = d['sexData']
            age_data = d['ageData']

            sex_data.each do |i|
                s = HkCustomerStatistics.new
                s.store_id = store_ids[0]
                s.user_type = user_type
                s.updated_at = h['dateTime']
                s.k = i['sex'] == 0 ? '女' : '男'
                s.v = i['value']
                s.t = '性别'
                cs << s
            end

            age_data.each do |i|
                s = HkCustomerStatistics.new
                s.store_id = store_ids[0]
                s.user_type = user_type
                s.updated_at = h['dateTime']
                s.k = i['name']
                s.v = i['value']
                s.t = '年龄段'
                cs << s
            end
        end
        HkCustomerStatistics.transaction do 
            cs.each do |s|
                s.save!
            end
        end
        $logger.info("保存了#{cs.size}条客群属性数据")
    end

    def self.sync_face_hour
        sync_access_token if $container['hk_access_token'].nil?
        store_ids = get_store

        url = HK_HOST + '/v1/customization/face/hour'
        h = {}
        h['storeId'] = store_ids[0]
        h['dateTime'] = (Time.now-3600*24).strftime('%Y-%m-%d')
        $logger.info("#{url} params: #{h}")
        body = HTTP.auth("Bearer #{$container['hk_access_token']}").get(url, :params=>h).body

        body_json = JSON.parse(body)
        raise '获取单日客群小时数据发生异常' if body_json['code'] != 200

        cs = []
        data = body_json['data']

        data.each do |d|
            user_type = d['userType']
            x_data = d['xData']
            y_data = d['yData']
            0.upto(x_data.size-1) do |i|
                s = HkCustomerStatistics.new
                s.store_id = store_ids[0]
                s.user_type = user_type
                s.updated_at = h['dateTime']
                s.k = x_data[i]
                s.v = y_data[i]
                s.t = '时间段'
                cs << s
            end
        end
        HkCustomerStatistics.transaction do 
            cs.each do |s|
                s.save!
            end
        end
        $logger.info("保存了#{cs.size}条客群小时数据")
    end
end